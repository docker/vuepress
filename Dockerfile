FROM node:16-bullseye-slim

# Install VuePress
RUN npm install -g vuepress

## VuePress image

The goal of this repository is to provide an image to build websites using [VuePress](https://www.npmjs.com/package/vuepress).
There is currently [Silkaj website](https://git.duniter.org/websites/doc_silkaj) using it.

The image is based on the [official Node.js images](https://hub.docker.com/_/node/?tab=description), based on Debian Bullseye Slim, with [VuePress](https://vuepress.vuejs.org/) on top.

